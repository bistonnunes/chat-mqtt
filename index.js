let http = require('http');
let fs = require('fs');

const port = process.env.PORT || 3000;

let handleRequest = (request, response) => {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    });
    fs.readFile('./demo.html', null, function (err, data) {
        if (err) console.log(err);
        response.writeHead(200, { 'Content-Type': 'text/html' });
        response.write(data);
        response.end();
    });
};

http.createServer(handleRequest).listen(port);